import json

from typing import List, Optional
from pydantic import BaseModel

from fastapi import APIRouter
from fastapi import File, Form, UploadFile
from fastapi import Depends, HTTPException
from fastapi import Query, Path

from sqlalchemy.orm import Session
import core.orm.models as models
from restlin.dependencies import db
from datetime import datetime
from random import randint


router = APIRouter()

class OutputTower(BaseModel):
    id: int
    health: int
    defense: int
    defender_count: int
    name: Optional[str] = None

    class Config:
        orm_mode = True


class InputDefender(BaseModel):
    nickname: str
    

class OutputDefender(BaseModel):
    nickname: Optional[str] = None
    # tower_name: Optional[str] = None
    server_uri : Optional[str] = None
    tower: Optional[OutputTower] = None
    enemy_tower: Optional[OutputTower] = None
        
        
class OutputSession(BaseModel):
    id: int
    time_created: str
    global_defender_count: int
    
    class Config:
        orm_mode = True


class TowerStaticData:
    def __init__(self, name, server_uri):
        self.name = name
        self.server_uri = server_uri
    # name: str
    # server_uri: str
    
towers_static_data = [TowerStaticData('Hocus', "http://localhost:666"),
                      TowerStaticData('Pocus', "http://localhost:999")]


def find_current_session(session_object):
    return session_object.query(models.Session).order_by(models.Session.time_created.desc()).limit(1).all()[0]


@router.post('/defenders/', summary="Create new defender", response_model=OutputDefender)
def create_defenders(input_defender:InputDefender, sess: Session = Depends(db)):
    """
    Creates new defender !
    """
    new_defender = models.Defender()
    new_defender.nickname = input_defender.nickname
    
    current_session = find_current_session(sess)
    
    # Na osnovu current_session odrediti koji od towera iz liste ima manje
    # defendera i njemu dodijeliti novog defendera
    
    my_tower = models.Tower
    enemy_tower = models.Tower
    my_static_data_id = 0
    
    if current_session.hocus_tower.defender_count > current_session.pocus_tower.defender_count:
        my_tower = current_session.pocus_tower
        enemy_tower = current_session.hocus_tower
        my_static_data_id = 1
    elif current_session.hocus_tower.defender_count < current_session.pocus_tower.defender_count:
        my_tower = current_session.hocus_tower
        enemy_tower = current_session.pocus_tower
        my_static_data_id = 0
    else:
        r = randint(0, 1)
        my_tower = (current_session.hocus_tower, current_session.pocus_tower)[r]
        enemy_tower = (current_session.hocus_tower, current_session.pocus_tower)[abs(r-1)]
        my_static_data_id = r
    
    new_defender.tower = my_tower
    
    #get tower url via os.environ['HOME']
    
    sess.add(new_defender)    
    sess.commit()
    
    ret = OutputDefender()
    ret.nickname = input_defender.nickname
    ret.tower = OutputTower.from_orm(my_tower)
    ret.tower.name = towers_static_data[my_static_data_id].name
    ret.server_uri = towers_static_data[my_static_data_id].server_uri
    ret.enemy_tower = OutputTower.from_orm(enemy_tower)
    ret.enemy_tower.name = towers_static_data[abs(my_static_data_id - 1)].name
    return ret


class InputTower(BaseModel):
    health: int

def return_tower_by_name(name, game_session):
    if name == 'Hocus':
        return game_session.hocus_tower
    elif name == 'Pocus':
        return game_session.pocus_tower
    else:
        raise ValueError('Name must be either Hocus or Pocus')
    
    
@router.put('/towers/{name}')
def update_tower(input_tower: InputTower, name: str = Path(...),sess: Session = Depends(db)):
    current_session = find_current_session(sess)
    tower = return_tower_by_name(name, current_session)
    tower.health = input_tower.health
    
    # emit to both tower servers


class UpdatedDefender(BaseModel):
    attack_points: int
    defense_points: int


@router.put('/defenders/{nickname}')
def update_defender(updated_defender: UpdatedDefender, nickname: str = Path(...), sess: Session = Depends(db)):
    defender = sess.query(models.Defender).get(nickname)
    defender.attack_points = updated_defender.attack_points
    defender.defense_points = updated_defender.defense_points
    
        
@router.get('/towers/{name}', response_model=OutputTower)
def get_tower_stats(name: str = Path(...), sess: Session = Depends(db)):
    output_tower = OutputTower.from_orm(return_tower_by_name(name, find_current_session(sess)))
    output_tower.name = name
    return output_tower

# @router.get('/sessions/', response_model=List[OutputSession])
# def list_sessions(
#         *,
#         sess: Session = Depends(db)):

#     # ret = []
#     sessions = sess.query(models.Session).all()

#     ret = [OutputSession.from_orm(session) for session in sessions]

#     return ret

