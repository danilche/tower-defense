"""

Revision ID: 1c1e42a45ce8
Revises: 3abd39b45672
Create Date: 2020-11-05 15:52:51.080649

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1c1e42a45ce8'
down_revision = '3abd39b45672'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
