"""

Revision ID: 25563a7bc755
Revises: c0796ee7c61c
Create Date: 2020-11-03 20:20:38.430081

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '25563a7bc755'
down_revision = 'c0796ee7c61c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('defenders', sa.Column('id_tower', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'defenders', 'towers', ['id_tower'], ['id'])
    op.add_column('towers', sa.Column('id_session', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'towers', 'sessions', ['id_session'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'towers', type_='foreignkey')
    op.drop_column('towers', 'id_session')
    op.drop_constraint(None, 'defenders', type_='foreignkey')
    op.drop_column('defenders', 'id_tower')
    # ### end Alembic commands ###
