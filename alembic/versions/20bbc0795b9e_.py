"""

Revision ID: 20bbc0795b9e
Revises: 25563a7bc755
Create Date: 2020-11-05 13:27:33.289835

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '20bbc0795b9e'
down_revision = '25563a7bc755'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('beers')
    op.drop_table('beerstyle')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('beerstyle',
    sa.Column('id', sa.BIGINT(), server_default=sa.text("nextval('beerstyle_id_seq'::regclass)"), autoincrement=True, nullable=False),
    sa.Column('name', sa.TEXT(), autoincrement=False, nullable=True),
    sa.PrimaryKeyConstraint('id', name='beerstyle_pkey'),
    postgresql_ignore_search_path=False
    )
    op.create_table('beers',
    sa.Column('id', sa.BIGINT(), autoincrement=True, nullable=False),
    sa.Column('id_style', sa.BIGINT(), autoincrement=False, nullable=True),
    sa.Column('name', sa.TEXT(), autoincrement=False, nullable=True),
    sa.Column('abv', sa.TEXT(), autoincrement=False, nullable=True),
    sa.Column('ibu', sa.TEXT(), autoincrement=False, nullable=True),
    sa.Column('srm', sa.TEXT(), autoincrement=False, nullable=True),
    sa.Column('upc', sa.TEXT(), autoincrement=False, nullable=True),
    sa.Column('descript', sa.TEXT(), autoincrement=False, nullable=True),
    sa.Column('last_mod', sa.TEXT(), autoincrement=False, nullable=True),
    sa.ForeignKeyConstraint(['id_style'], ['beerstyle.id'], name='beers_id_style_fkey'),
    sa.PrimaryKeyConstraint('id', name='beers_pkey')
    )
    # ### end Alembic commands ###
