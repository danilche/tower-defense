import os
import csv
import argparse
import sys
import time
from datetime import datetime

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from core.orm import create_session
from core.orm.models import Session, Tower

from sqlalchemy import exc
import alembic.config

def check_connection():
    sess = create_session()
    try:
        a = sess.execute("SELECT NOW()")
        return True
    except exc.OperationalError as e:
        return False

if __name__ == '__main__':

    # Wait for connection
    print("Trying to connect to db ...")
    while not check_connection():
        time.sleep(3)
        print("Retrying ...")
    print("Connection succeded !")

    # Check migrations
    alembic.config.main(argv=[
        '--raiseerr',
        'upgrade', 
        'head'
    ])
  
    sess = create_session()
    
    current_session = Session()
    current_session.global_defender_count = 0
    current_session.time_created = datetime.now()
    
    current_session.hocus_tower = Tower()
    current_session.hocus_tower.defender_count = 0
    current_session.hocus_tower.health = 5000
    current_session.hocus_tower.defense = 0
    
    current_session.pocus_tower = Tower()
    current_session.pocus_tower.defender_count = 0
    current_session.pocus_tower.health = 5000
    current_session.pocus_tower.defense = 0

    # pocus = Tower()
    # pocus.defender_count = 0
    # pocus.health = 5000
    # pocus.defense = 0
    # pocus.session = current_session
    # sess.add(pocus)
   
    # current_session.towers = [hocus, pocus]
    
    sess.add(current_session)
    sess.commit()
    
    print("DB init succeeded!")