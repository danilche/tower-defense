import socketio

sio = socketio.AsyncServer(async_mode='asgi', cors_allowed_origins='*')
app = socketio.ASGIApp(sio)


@sio.event
def connect(sid, environ):
    print("connect ", sid)

@sio.event
def disconnect(sid):
    print('disconnect ', sid)
    
@sio.event
def attack(sid):
    print('attack ', sid)
    
@sio.event
def defend(sid):
    print('defend ', sid)
