# coding: utf-8
from sqlalchemy import BigInteger, Boolean, CHAR, Column, DateTime, Date, ForeignKey, ForeignKeyConstraint, \
    Integer, String, Table, Text, UniqueConstraint, Numeric, text, Enum
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import extract
import datetime
from datetime import datetime
import enum

Base = declarative_base()
metadata = Base.metadata
    
class Tower(Base):
    __tablename__ = 'towers'

    id = Column(Integer, primary_key=True, autoincrement=True)
    health = Column(Integer)
    defense = Column(Integer)
    id_session = Column(ForeignKey('sessions.id'))
    defender_count = Column(Integer)
    
    
class Session(Base):
    __tablename__ = 'sessions'

    id = Column(Integer, primary_key=True, autoincrement=True)
    time_created = Column(DateTime)
    global_defender_count = Column(Integer)
    hocus_tower_id = Column(ForeignKey('towers.id'))
    pocus_tower_id = Column(ForeignKey('towers.id'))
    
    hocus_tower = relationship('Tower', foreign_keys=[hocus_tower_id])
    pocus_tower = relationship('Tower', foreign_keys=[pocus_tower_id])
    

class Defender(Base):
    __tablename__ = 'defenders'

    nickname = Column(String, primary_key=True)
    attack_points_generated = Column(Integer)
    defense_points_generated = Column(Integer)
    id_tower = Column(ForeignKey('towers.id'))
    
    tower = relationship('Tower')
    

# Tower.session = relationship('Session')
# Session.towers = relationship('Tower',  back_populates = "session")